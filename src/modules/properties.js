const UPDATE = 'reactProject/properties/UPDATE';

export default (state = {}, action = {}) => {
    return action.type === UPDATE ? action.payload : state;
}