import * as React from 'react';

export default class PageNotFound extends React.Component {
    render() {
        return (
            <div className='h-100 row align-items-center'>
                <div className='col-12' align='center'>
                    <h3>Hello, World!</h3>
                </div>
            </div>
        );
    }
}