import thunk from 'redux-thunk';
import {applyMiddleware, compose, createStore} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import {createBrowserHistory} from 'history';

import localProps from '../properties/environments/local';
import devProps from '../properties/environments/dev';
import demoProps from '../properties/environments/demo';
import stagingProps from '../properties/environments/staging';
import prodProps from '../properties/environments/prod';
import rootReducer from '../modules/reducer-index';

const browserHistory = createBrowserHistory();

let properties;
let enableReduxDevTools = true;
switch (window.location.hostname) {
    case 'fill me in for dev!':
        properties = devProps;
        break;
    case 'fill me in for demo!':
        properties = demoProps;
        break;
    case 'fill me in for staging!':
        properties = stagingProps;
        break;
    case 'fill me in for prod!':
        properties = prodProps;
        enableReduxDevTools = false;
        break;
    default:
        properties = localProps;
}

let composeEnhancers;
if (enableReduxDevTools) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({trace: true, traceLimit: 25}) || compose;
} else {
    composeEnhancers = compose;
}

const appStore = createStore(
    rootReducer,
    {
        properties: properties
    },
    composeEnhancers(applyMiddleware(routerMiddleware(browserHistory), thunk))
);

if (module.hot && module.hot.accept) {
    module.hot.accept('../modules/reducer-index', () => {
        const nextRootReducer = require('../modules/reducer-index');
        appStore.replaceReducer(nextRootReducer);
    });
}

export const store = appStore;
export const history = browserHistory;